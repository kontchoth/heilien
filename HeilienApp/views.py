from django.shortcuts import render, render_to_response, redirect
from django.template import RequestContext

def index(request):
	params = {
		'title': 'Home'
	}
	return render_to_response('heilienapp.index.html', params, RequestContext(request))
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class DiabeteData(models.Model):
	'''
	DB object containing Static Diabetes Data.
	This table WILL NOT be presented to the user
	and neither will it have an API for it since it 
	is supposed to be an internal table not exposed
	to the user.
	'''
	name = models.CharField(max_length=100, null=False, blank=False)
	value = models.IntegerField(default=0)

	def __str__(self):
		return '{}:{}'.format(self.name, self.value)

	def get_name(self):
		return self.name

	def get_value(self):
		return self.value


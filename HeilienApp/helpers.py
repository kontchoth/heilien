"""
This file contains all helpers method used for the heilienApp 
intermediate steps
"""
from .exceptions import InvalidFileFormat
from .models import DiabeteData

def _validate_line(line):
	'''
	This function validate line in a file.
	validation is correct if each line contains
	4 elements. 
	It does not matter what those elements are
	if a column is not valid, it will be catched 
	by another checker down the road
	'''
	words = line.split()
	if len(words) != 4:
		return None
	return words

def _get_object_value(list_object_names, object_name):
	'''
	This function return the value of an object
	@list_object_name -- list of all the valid objects present
							in the system
	@object_name  -- name of the object to find the value
	@Return :
		Return the value of the object name if present in the system
		otherwise raise an ObjectNotFound Exception
	'''
	obj = DiabeteData.objects.get(name__iexact=object_name)
	return obj.get_value()

def read_user_file(filepath):
	'''
	This function read from the file and manipulate the file
	'''
	content = []
	with open(filepath, 'r') as i_file:
		for line in i_file:
			words = _validate_line(line)
			if words is None:
				raise InvalidFileFormat('{}'.format(line))
			# replace 1st and 3rd cols by their numbers
			words[0] = _get_object_value(words[0])
			words[2] = _get_object_value(words[2])
			content.append(words)
	return content


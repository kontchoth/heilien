var $ = require('jquery');
var cookie = require('js-cookie');
var _ = require('lodash');
function csrfSafeMethod(method){
  // these http methods does not require CSRF protection
  return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
var csrftoken = cookie.get('csrftoken');
var Utils = {
  setupCSRFToken: function(){
    $.ajaxSetup({
      beforeSend: function(xhr, settings){
        if (!csrfSafeMethod(settings.type) && !this.crossDomain){
          xhr.setRequestHeader('X-CSRFToken', csrftoken);
        }
      }
    });
  },
  getCSRFToken: function(){
    return csrftoken;
  },
  ajaxCall(options){
    return $.ajax({
      url: options.url,
      type: options.type,
      datatype: options.dataType,
      data: options.data,
      processData: options.proccessData == undefined ? true:options.proccessData
    });
  },
  staticServer: function(){
    return 'http://localhost:3001/assets/';
  }
};
module.exports = Utils;
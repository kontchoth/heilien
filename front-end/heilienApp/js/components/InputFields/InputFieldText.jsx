import CSSModules from 'react-css-modules';
import styles from './InputFieldText.css';
var React = require('react');
var _ = require('lodash');
var $ = require('jquery');
const VALID_TYPES = ['email', 'password', 'text', 'phone'];
const DATA_MASKS = {
	phone: '(___) ___-____',
}
var InputFieldText = React.createClass({
	propTypes: {
		label: React.PropTypes.string.isRequired,
		id: React.PropTypes.string.isRequired,
		required: React.PropTypes.bool,
		error: React.PropTypes.string,
		type: React.PropTypes.string
	},
	getDefaultProps: function() {
		return {
			required: false,
			error: 'Invalid Field',
			type: 'text'
		}
	},
	getInitialState: function() {
		return {
			has_error: false
		}	
	},
	isValidEmail: function(email){
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    	return re.test(email);
	},
	isValidText: function(value) {
		return value.trim() !== '';
	},
	isValidPhone: function(value){
		var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
		var phoneno_inter = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
		if(value.match(phoneno) || value.match(phoneno_inter)) {
			return true;
		}
  		return false;
	},
	validateData: function(){
		let {required, type} = this.props;
		var validators = {
			'email': this.isValidEmail,
			'text': this.isValidText,
			'password': this.isValidText,
			'phone': this.isValidPhone,
		};
		var index = _.indexOf(VALID_TYPES, type);
		var data = this.refs.inputTag.value.trim();
		if (required) {
			if (index >= 0) {
				if (!validators[type](data)) {
					this.setState({has_error: true});
					return;
				}
			}
			if (data === '') {
				this.setState({has_error: true});
				return;
			}
		} else {
			// in case not required and value given
			// check for validation of the value
			if (data !== ''){
				if (index > -1) {
					if (!validators[type](data)){
						this.setState({has_error: true});
						return;
					}
				}
			}
		}
		this.setState({has_error: false});
	},
	getValue: function(){
		return this.refs.inputTag.value;
	},
	render: function() {
		let {required, type, label, error, id} = this.props;
		let {has_error} = this.state;
		var styleNameError = has_error ? 'has-error' : '';
		return (
			<div styleName='input-container'>
				<label>{label}</label>
				{
					required ? 
						<input type={type} id={id} ref='inputTag' required onBlur={this.validateData} styleName={styleNameError}/>
					:
						<input type={type} id={id} ref='inputTag' onBlur={this.validateData} styleName={styleNameError}/>	
				}
			</div>
		);
	}
});
module.exports = CSSModules(InputFieldText, styles);
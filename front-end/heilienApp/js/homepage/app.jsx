import CSSModules from 'react-css-modules';
import styles from './app.css';
var React = require('react');
var $ = require('jquery');
var utils = require('../common/utils');
var LinkedStateMixin = require('react-addons-linked-state-mixin');
var InputFieldText = require('../components/InputFields/InputFieldText');
var FlatButton = require('../components/Buttons/FlatButton');
var CheckBox = require('../components/InputFields/CheckBox');
var NavBar = require('../components/Navigations/HeilienNav/HeilienAppNav');
var App = React.createClass({
	mixins:[LinkedStateMixin],
	getInitialState: function(){
		return {
		};
	},
	componentDidMount: function(){
		utils.setupCSRFToken();
	},
	render: function(){
		return( 
			<div styleName='main-area'>
				<NavBar />

			</div>
		);	
	}
	
});
module.exports = CSSModules(App, styles);
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

from contextlib import contextmanager, nested
import platform
import sys, os

from fabric.api import local, task, warn_only
from fabric.context_managers import hide, settings
from fabric.contrib import django

# Setup colors.
try:
    from colorama import Back, Fore, Style, init
    from django.conf import settings as django_settings_py
except ImportError:
    local("pip install colorama==0.2.5 Django==1.10.1")
    from colorama import Back, Fore, Style, init
    from django.conf import settings as django_settings_py

# Import settings.py
django.settings_module('heilien.settings')

init(autoreset=True)

APP_LIST = [
    app for app in django_settings_py.INSTALLED_APPS
    if not app.startswith("django.")
]


IS_WIN = platform.system().lower().startswith("win")
IS_TTY = sys.stdout.isatty()

STATUS_MARK = u'\u2712' * IS_TTY
X_MARK = u'\u2718' * IS_TTY
CHECK_MARK = u'\u2714' * IS_TTY
WARNING_MARK = u"\u26A0" * IS_TTY
NOTE_MARK = u'\u2710' * IS_TTY


def W(string, prefix=" "):
    """Returns "" if this platform is WIN."""
    return "" if IS_WIN else prefix + string


def hide_warnings():
    return settings(hide('warnings'))


def _fprint(bg, status, message):
    print(Fore.WHITE + Style.BRIGHT + bg + " " + status + " ", end="")
    print(Fore.WHITE + Style.BRIGHT + " " + message)


def warn(message):
    _fprint(Back.YELLOW, "WARNING", message + W(Fore.YELLOW + WARNING_MARK))


def success(message):
    _fprint(Back.GREEN, "SUCCESS", message + W(Fore.GREEN + CHECK_MARK))


def note(message):
    _fprint(Back.CYAN, " NOTE  ", message + W(Fore.CYAN + NOTE_MARK))


def error(message):
    _fprint(Back.RED, " ERROR ", message + W(Fore.RED + X_MARK))


@contextmanager
def announce(what):
    """Status decorate an event."""
    announcer_dict = {}
    print(Fore.WHITE + Back.MAGENTA + Style.BRIGHT + "  START  ", end="")
    print(Fore.WHITE + Style.BRIGHT + W(STATUS_MARK) + " " + what)
    try:
        yield announcer_dict
    except:
        error(what)
        raise  # let fabric handle this.
    else:
        if not announcer_dict: success(what)
        else: warn(what)


def start_django_local(addr, debug):
    with nested(announce("Django: LAUNCHING...")):
        with announce("Django: Making Migrations"):
            local('python manage.py makemigrations')
            for app in APP_LIST:
                local('python manage.py makemigrations {}'.format(app))
                
        with announce("Django: Migrating"):
            local('python manage.py migrate')

        with announce("Django: Collecting static"):
            local('python manage.py collectstatic --noinput')

        run_server_command = " ".join([
            "python manage.py runserver",
            addr,
            ("--insecure" if not debug else ""),
        ])

        cmd = " ".join(['nohup', run_server_command, '> runserver.log &'])
        if IS_WIN:
            cmd = " ".join(['start /min', '"Django"', run_server_command])

        run_server_announce = "".join([
            "Django: Launching ", ("[DEBUG]" if debug else "[PROD]"), " Server"
        ])
        with announce(run_server_announce):
            local(cmd)

        cmds = []
        apps = next(os.walk(django_settings_py.STATIC_ROOT))[1]
        for app in apps:
            cmd = " ".join([
                "ln -s ",
                django_settings_py.STATIC_ROOT + '/{}'.format(app),
                django_settings_py.STATIC_ROOT + '../../{}'.format(app)
            ])
            cmds.append(cmd)

        if debug:
            for cmd, app in zip(cmds, apps):
                run_server_announce = "".join([
                    "Django: Create SymLink for Django {} application CSS ".format(app)
                ])
                with announce(run_server_announce):
                    local(cmd)

        note("=" * 80)
        if not IS_WIN:
            note(
                'Log of runserver command is being redirected to ./runserver.log'
            )
            note('To see them, run: $ tail -f runserver.log')
        else:
            note('Django running in a minimized command prompt shell.')
            note('View logs there.')
        note("=" * 80)


def stop_react_local():
    cmd = "ps aux | grep 'node server.js' | awk '{print $2}'"
    if IS_WIN:
        cmd = 'taskkill /f /fi "IMAGENAME eq node.exe" /fi "WINDOWTITLE eq Webpack"'
    with nested(announce("Node: Stopping Webpack server"), hide_warnings()):
        res = local(cmd, capture=True)
        if IS_WIN: return
        res = res.split('\n')
        with warn_only():
            for pid in res:
                local('kill -9 {}'.format(pid), capture=True)


def remove_app_files():
    try:
        apps = next(os.walk(django_settings_py.STATIC_ROOT))[1]
        cmds = []
        for app in apps:
            cmd = " ".join([
                "rm -rf ",
                django_settings_py.STATIC_ROOT + '../../{}'.format(app)
            ])
            cmds.append(cmd)
        if debug:
            for cmd, app in zip(cmds, apps):
                run_server_announce = "".join([
                    "Django: Deleting SymLink for Django {} application CSS ".format(app)
                ])
                with announce(run_server_announce):
                    local(cmd)
    except:
        pass


def stop_django_local(debug=True):
    cmd = "ps aux | grep 'manage.py runserver' | awk '{print $2}'"
    if IS_WIN:
        cmd = 'taskkill /f /fi "IMAGENAME eq python.exe" /fi "WINDOWTITLE eq Django"'
    with nested(announce("Django: Stopping server"), hide_warnings()):
        if IS_WIN:
            local(cmd)
            local(cmd)  # django kicks off a subprocess child.
            return

        res = local(cmd, capture=True)
        res = res.split('\n')
        with warn_only():
            for pid in res:
                local('kill -9 {}'.format(pid), capture=True)
    remove_app_files()    

    

def start_react_local(debug=True):
    with announce("Node: Compiling " + ("debug"
                                        if debug else "prod") + " modules"):
        cmd_template = "npm run build-{}"
        if debug:
            local(cmd_template.format("debug"))
        else:
            local(cmd_template.format("prod"))

    if not debug: return  # django will serve if prod.

    with announce("Node: Starting Webpack Server"):
        if IS_WIN:
            local('start /min "Webpack" node server.js')
        else:
            local('nohup node server.js > webpack.log &')

        note("=" * 80)

        if IS_WIN:
            note('Node currently running in a minimized command prompt shell.')
            note('Follow logs there.')
        else:
            note('Logs of webpack command is being been redirected'
                 'to ./webpack.log')
            note('To view them, run: $ tail -f webpack.log')
        note("=" * 80)

def start(addr, debug):
    stop()
    init()
    start_react_local(debug=debug)
    start_django_local(addr=addr, debug=debug)



@task
def init():
  """Installs all python and npm dependencies."""
  with announce("FAB: Initialization") as fab_announcer:
    with announce("Django: Installing python requirements") as announcer:
      cmd = "pip install -r requirements.txt"
      if IS_WIN:
          cmd = "pip install -r win_requirements.txt"
      res = local(cmd, capture=True)
      if res.return_code != 0: announcer["error"] = True

    with announce("Node: Installing npm modules") as announcer:
      res1 = local('npm install', capture=True)
      if res1.return_code != 0: announcer["error"] = True

    init_passed = res.return_code == 0 and res1.return_code == 0
    if not init_passed:
      fab_announcer["error"] = True
    return init_passed



@task(aliases=["debug", "d"])
def start_debug(addr="127.0.0.1:8000"):
  """
  Starts our debug servers.

  start_debug::addr="127.0.0.1:8000"

  """
  with announce("FAB: START-DEBUG"):
      start(addr=addr, debug=True)


@task(aliases=["prod", "p"])
def start_prod(addr="127.0.0.1:8000"):
  """Starts our production stack."""
  with announce("FAB: START-PROD"):
      start(addr=addr, debug=False)


@task
def stop(method='local'):
  """Stops our stack."""
  with nested(announce("FAB: STOP"), settings(warn_only=False)):
      stop_django_local()
      stop_react_local()


var path = require("path");
var webpack = require('webpack');
var BundleTracker = require('webpack-bundle-tracker');
var config = require('./webpack.base.config.js');
// Get server info
sinfo = config.server_info;
// Set output path for generated files. Must be relative to $server_root.
var outputPath = path.join(sinfo.server_root, 'bundles/');
config.output.path = path.join(__dirname, outputPath);
// Get the full host name. e.g. http://localhost:3000
full_hostname = "".concat(
  sinfo.connection_type, "://", sinfo.hostname + ":", sinfo.port);
// Set public path for django's webpack loader
config.output.publicPath = full_hostname.concat(outputPath)
// Enable Live Update
entriesWithLiveUpdates = {}
for(entry in config.entry) {
  entriesWithLiveUpdates[entry] = [
    'webpack-dev-server/client?' + full_hostname,
    'webpack/hot/only-dev-server',
    config.entry[entry]
  ]
}
config.entry = entriesWithLiveUpdates;
// Add HotModuleReplacementPlugin and BundleTracker plugins
config.plugins = config.plugins.concat([
  // Live update react
  new webpack.HotModuleReplacementPlugin(),
  // don't reload if there is an error
  new webpack.NoErrorsPlugin(),
  // Generates a stats file which django will consume.
  new BundleTracker({filename: './webpack-stats-debug.json'})
]);
config.module.loaders.push({
  test: /\.jsx?$/, // to transform JSX into JS
  exclude: /node_modules/,
  loader: 'babel-loader',
  query: {
    plugins: ["transform-runtime", "transform-decorators-legacy"],
    presets: ["es2015", "react", "stage-0"]
  }
});

config.module.loaders.push({
  test: /\.css$/,
  loaders: ['style?sourceMap', 'css?modules&importLoaders=1&localIdentName=[path]___[name]__[local]___[hash:base64:5]']
})
module.exports = config;
